import generateMath from '../generate-math';

const operations = ['+', '-', '*', '/'];

describe('generate-math.js', () => {
  describe('getProblemSet()', () => {
    describe('when operation is "+"', () => {
      const operation = '+';
      const problemSet = generateMath.getProblemSet(operation);

      it('should provide an addition operation', () => {
        expect(problemSet).toHaveProperty('operation');
        expect(problemSet.operation).toEqual(operation);
      });

      it('should have 2 operands', () => {
        expect(problemSet).toHaveProperty('operands');
        expect(problemSet.operands).toHaveLength(2);
      });

      it('should provide a valid solution', () => {
        expect(problemSet).toHaveProperty('solution');
        expect(problemSet.operands[0] + problemSet.operands[1])
          .toEqual(problemSet.solution);
      });
    });

    describe('when operation is "-"', () => {
      const operation = '-';
      const problemSet = generateMath.getProblemSet(operation);

      it('should provide a subtraction operation', () => {
        expect(problemSet).toHaveProperty('operation');
        expect(problemSet.operation).toEqual(operation);
      });

      it('should have 2 operands', () => {
        expect(problemSet).toHaveProperty('operands');
        expect(problemSet.operands).toHaveLength(2);
      });

      it('should provide a valid solution', () => {
        expect(problemSet).toHaveProperty('solution');
        expect(problemSet.operands[0] - problemSet.operands[1])
          .toEqual(problemSet.solution);
      });
    });

    describe('when operation is "*"', () => {
      const operation = '*';
      const problemSet = generateMath.getProblemSet(operation);

      it('should provide a multiplication operation', () => {
        expect(problemSet).toHaveProperty('operation');
        expect(problemSet.operation).toEqual(operation);
      });

      it('should have 2 operands', () => {
        expect(problemSet).toHaveProperty('operands');
        expect(problemSet.operands).toHaveLength(2);
      });

      it('should provide a valid solution', () => {
        expect(problemSet).toHaveProperty('solution');
        expect(problemSet.operands[0] * problemSet.operands[1])
          .toEqual(problemSet.solution);
      });
    });

    describe('when operation is "/"', () => {
      const operation = '/';
      const problemSet = generateMath.getProblemSet(operation);

      it('should provide a division operation', () => {
        expect(problemSet).toHaveProperty('operation');
        expect(problemSet.operation).toEqual(operation);
      });

      it('should have 2 operands', () => {
        expect(problemSet).toHaveProperty('operands');
        expect(problemSet.operands).toHaveLength(2);
      });

      it('should provide a valid solution', () => {
        expect(problemSet).toHaveProperty('solution');
        expect(problemSet.operands[0] / problemSet.operands[1])
          .toEqual(problemSet.solution);
      });
    });
  });

  describe('getProblemString()', () => {
    operations.forEach(operation => {
      const problemSet = generateMath.getProblemSet(operation);
      const problemString = generateMath.getProblemString(problemSet);
      const expected = (
        problemSet.operands[0] + ' ' +
        problemSet.operation + ' ' +
        problemSet.operands[1]
      );

      expect(problemString).toEqual(expected);
    });
  });

  describe('getRandomOperation()', () => {
    it('should generate at least one of each operation', () => {
      let obj = {};
      operations.forEach(operation => {
        obj[operation] = 0;
      });

      for (let i = 0; i < 1000; i++) {
        const operation = generateMath.getRandomOperation();
        obj[operation] += 1;
      }

      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          expect(obj[key]).toBeGreaterThan(0);
        }
      }
    });
  });
});
