import getRandomNumber from './get-random-number';

export default {
  getProblemSet: (operation) => {
    const additionMin = 0;
    const additionMax = 99;
    const subtractionMin = 0;
    const subtractionMax = 99;
    const multiplicationMin = 2;
    const multiplicationMax = 10;
    const divisionMin = 1;
    const divisionMax = 100;

    let obj = {
      operands: [],
      operation: operation,
      solution: 0
    };

    switch (operation) {
      case '+':
        obj.operands.push(getRandomNumber(additionMin, additionMax));
        obj.operands.push(getRandomNumber(additionMin, additionMax));
        obj.solution = obj.operands[0] + obj.operands[1];
        break;

      case '-':
        obj.operands.push(getRandomNumber(subtractionMin, subtractionMax));
        obj.operands.push(getRandomNumber(subtractionMin, subtractionMax));
        obj.solution = obj.operands[0] - obj.operands[1];
        break;

      case '*':
        obj.operands.push(getRandomNumber(multiplicationMin, multiplicationMax));
        obj.operands.push(getRandomNumber(multiplicationMin, multiplicationMax));
        obj.solution = obj.operands[0] * obj.operands[1];
        break;

      case '/':
        const x = getRandomNumber(divisionMin, divisionMax);
        const y = getRandomNumber(divisionMin, divisionMax);
        if (x > y) {
          obj.operands.push(x);
          obj.operands.push(y);
        }
        else {
          obj.operands.push(y);
          obj.operands.push(x);
        }
        obj.solution = obj.operands[0] / obj.operands[1];
        break;

      default:
        break;
    }

    return obj;
  },
  getProblemString (problemSet) {
    return (
      problemSet.operands[0] + ' ' +
      problemSet.operation + ' ' +
      problemSet.operands[1]
    );
  },
  getRandomOperation () {
    const operations = ['+', '-', '*', '/'];
    const num = getRandomNumber(0, 3);

    return operations[num];
  }
};
